const Element = require('InfinniUI').Element;
const FlexContainerControl = require('../controls/FlexContainer/FlexContainerControl');

class FlexContainer extends Element{
  constructor(parent) {
    super(parent);
  }

  createControl ( parent ) {
    return new FlexContainerControl(parent);
  }

  /**
   *
   * @param {string} name
   * @param {*} value
   */
  setContainerProperty(name, value) {
    this.control.set(name, value);
  }

  /**
   *
   * @param {string} name
   * @return {*}
   */
  getContainerProperty(name) {
    return this.control.get(name)
  }

  setChildren(value) {
    this.control.set('children', value);
  }

  getChildren() {
    return this.control.get('children');
  }

}



module.exports = FlexContainer;
