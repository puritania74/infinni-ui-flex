const Element = require('InfinniUI').Element;
const FlexItemControl = require('../controls/FlexItem/FlexItemControl');

class FlexItem extends Element {

  constructor( parent ) {
    super( parent );
  }

  createControl( parent ) {
    return new FlexItemControl( parent );
  }

  /**
   *
   * @param {string} name
   * @param {*} value
   */
  setItemProperty(name, value) {
    this.control.set(name, value);
  }

  /**
   *
   * @param {string} name
   * @return {*}
   */
  getItemProperty(name) {
    return this.control.get(name)
  }

  /**
   *
   * @param {Function} value
   */
  setContent(value) {
    this.control.set('content', value);
  }

  /**
   * @return {Function}
   */
  getContent() {
    return this.control.get('content');
  }

}


module.exports = FlexItem;
