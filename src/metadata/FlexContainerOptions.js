/**
 * @description transform to align-items
 * @enum {string}
 */
const ChildrenAlign = {
  Start: 'Start',
  End: 'End',
  Center: 'Center',
  Baseline: 'Baseline',
  Stretch: 'Stretch'
};

/**
 * @description transform to align-content
 * @enum {string}
 */
const ContainerAlign = {
  Start: 'Start',
  End: 'End',
  Center: 'Center',
  SpaceBetween: 'SpaceBetween',
  SpaceAround: 'SpaceAround',
  Stretch: 'Stretch'
};

/**
 * @description transform to justify-content
 * @enum {string}
 */
const Justify = {
  Start: 'Start',
  End: 'End',
  Center: 'Center',
  SpaceBetween: 'SpaceBetween',
  SpaceAround: 'SpaceAround',
  Stretch: 'Stretch'
};

/**
 * @description transform to flex-direction
 * @enum {string}
 */
const Direction = {
  Row: 'Row',
  RowReverse: 'RowReverse',
  Column: 'Column',
  ColumnReverse: 'ColumnReverse'
};

/**
 * @description transform to flex-wrap
 * @enum {string}
 */
const Wrap = {
  NoWrap: 'NoWrap',
  Wrap: 'Wrap',
  WrapReverse: 'WrapReverse'
};

module.exports = {
  Direction,
  Wrap,
  Justify,
  ContainerAlign,
  ChildrenAlign
};