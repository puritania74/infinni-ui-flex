/**
 * @description transform to align-self
 * @enum {string}
 */
const Align = {
  Auto: 'Auto',
  Start: 'Start',
  End: 'End',
  Center: 'Center',
  Baseline: 'Baseline',
  Stretch: 'Stretch'
};

module.exports = {Align};