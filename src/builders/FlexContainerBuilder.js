const ElementBuilder = require('InfinniUI').ElementBuilder;
const FlexContainer = require('../elements/FlexContainer');

class FlexContainerBuilder extends ElementBuilder {

  constructor() {
    super();
  }

  createElement ( params ) {
    return new FlexContainer(params.parent);
  }

  applyMetadata ( params ) {
    super.applyMetadata(params);
    this.applyProperties(params);
    this.applyChildren(params);
  }

  /**
   * @protected
   */
  applyProperties ( params ) {
    let element = params.element;
    let metadata = params.metadata;

    let attributes ='Direction,Wrap,Justify,Align,ChildrenAlign'.split(',');

    Object.keys(metadata)
      .filter(name => attributes.some(prop => name.indexOf(prop) === 0))
      .forEach(name => element.setContainerProperty(this.lowerFirstSymbol(name), metadata[name]));

    return this;
  }

  /**
   * @protected
   * @param params
   */
  applyChildren ( params ) {
    const metadata = params.metadata;
    const element = params.element;
    const children = metadata['Children'] || [];

    const instances = children.map(childrenMetadata => params.builder.build(childrenMetadata, {
      parent: element,
      parentView: params.parentView,
      basePathOfProperty: params.basePathOfProperty
    }));

    element.setChildren(instances);
  }

}


module.exports = FlexContainerBuilder;
