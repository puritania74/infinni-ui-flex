const ElementBuilder = require('InfinniUI').ElementBuilder;
const FlexItem = require('../elements/FlexItem');


/**
 * @class
 */
class FlexItemBuilder extends ElementBuilder{

  constructor() {
    super();
  }

  createElement ( params ) {
    return new FlexItem(params.parent);
  }

  applyMetadata ( params ) {
    super.applyMetadata(params);

    this
      .applyProperties(params)
      .applyContent(params);
  }

  /**
   * @protected
   * @param params
   * @return {FlexItemBuilder}
   */
  applyProperties ( params ) {
    const metadata = params.metadata;
    const element = params.element;

    let attributes = 'Align,Order,Grow,Shrink,Basis'.split(',');

    Object.keys(metadata)
      .filter(name => attributes.some(prop => name.indexOf(prop) === 0))
      .forEach(name => element.setItemProperty(this.lowerFirstSymbol(name), metadata[name]));

    return this;
  }

  /**
   * @protected
   * @param params
   * @return {FlexItemBuilder}
   */
  applyContent ( params ) {
    const metadata = params.metadata;
    const element = params.element;
    const contentMetadata = metadata['Content'];

    element.setContent(this.getContentFactory(contentMetadata, params));

    return this;
  }

  /**
   * @protected
   * @param metadata
   * @param params
   * @return {function()}
   */
  getContentFactory ( metadata, params ) {
    const element = params.element;
    const parentView = params.parentView;
    const builder = params.builder;

    if (!metadata) {
      return () => {};
    }

    return (  ) => {
      let argumentForBuilder = {
        parent: element,
        parentView: parentView
      };

      return builder.build( metadata, argumentForBuilder );
    };
  }
}

module.exports = FlexItemBuilder;