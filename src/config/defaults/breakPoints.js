module.exports = [
  {
    'xs': [
      'screen',
      // { 'max-width': '599px' }
    ]
  },
  {
    'sm': [
      'screen',
      { 'min-width': '600px' },
      // { 'max-width': '959px' }
    ]
  },
  {
    'md': [
      'screen',
      { 'min-width': '960px' },
      // { 'max-width': '1279px' }
    ]
  },
  {
    'lg': [
      'screen',
      { 'min-width': '1280px' },
      // { 'max-width': '1919px' }
    ]
  },
  {
    'xl': [
      'screen',
      { 'min-width': '1920px' },
      // { 'max-width': '5000px' }
    ]
  }
];