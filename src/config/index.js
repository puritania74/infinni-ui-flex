const Config = require('./Config');
const breakPointsConfig = require('./defaults/breakPoints');

const config = new Config();
config.set('breakPoints', breakPointsConfig);

module.exports = config;
