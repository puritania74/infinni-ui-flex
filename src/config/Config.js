class Config {

  constructor() {
    this.data = Object.create(null);
  }

  set(name, value) {
    this.data[name] = value
  }

  get(name, defaultValue) {
    return (name in this.data) ? this.data[name] : defaultValue;
  }

}

module.exports = Config;