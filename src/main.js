const ApplicationBuilder = require('InfinniUI').ApplicationBuilder;

const FlexContainerBuilder = require('./builders/FlexContainerBuilder');
const FlexContainer = require('./elements/FlexContainer');
const FlexItemBuilder = require('./builders/FlexItemBuilder');
const FlexItem = require('./elements/FlexItem');

const FlexContainerOptions = require('./metadata/FlexContainerOptions');
const FlexItemOptions = require('./metadata/FlexItemOptions');

const config = require('../src/config');

ApplicationBuilder.addToRegisterQueue('FlexContainer', new FlexContainerBuilder());
ApplicationBuilder.addToRegisterQueue('FlexItem', new FlexItemBuilder());

module.exports = {FlexContainer, FlexItem, FlexContainerOptions, FlexItemOptions, config};