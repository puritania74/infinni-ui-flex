const Model = require('./StyleModel');



class Style {

  get el() {
    if (!this._el) {
      this._el = this.createStyleElement();
    }
    return this._el;
  }

  /**
   *
   * @param {string} guid
   * @param {Array<Function>} attributeStrategies
   */
  constructor(guid, attributeStrategies) {
    this.model = new Model(guid, attributeStrategies);
  }

  setAttributes(attributes) {
    this.model.attributes = attributes
  }


  render() {
    let el = this.el;
    //remove all child nodes
    while(el.firstChild) {
      el.removeChild(el.firstChild);
    }

    let styleText = document.createTextNode(this.model.css);
    el.appendChild(styleText);
  }

  /**
   * @protected
   * @return {Element}
   */
  createStyleElement() {
    let node = document.createElement('style');
    node.type = "text/css";
    return node;
  }


}

module.exports = Style;