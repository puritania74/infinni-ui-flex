const config = require('../../config');
const BreakPoints = require('../../mediaQuery/BreakPoints');

const COMMON_STYLE = 'COMMON';

class StyleCompiler {

  constructor(guid, attributeStrategies) {
    this.guid = guid;
    this.strategies = {};
    this.registerAttributeStrategies(attributeStrategies);
    this.breakPoints = new BreakPoints(config.get('breakPoints'));
  }

  compile(attributes, separator) {
    let data = {};

    Object.keys(attributes)
      .forEach(name => {
        let [prop, size = COMMON_STYLE] = name.split(separator);
        let style = data[size] = data[size] || {};
        style[prop] = attributes[name];
      });

    let css = this.compileCommonStyle(data);
    css += this.compileMediaQueryStyle(data);

    return css;
  }


  /**
   * @protected
   * @param strategies
   */
  registerAttributeStrategies(strategies) {
    Object.keys(strategies).forEach(name => this.strategies[name] = strategies[name]);
  }


  /**
   * @protected
   * @param {Object} data
   * @return {string}
   */
  compileCommonStyle(data) {
    return this.compileAttributes(data[COMMON_STYLE])
  }

  /**
   * @protected
   * @param data
   * @return {string}
   */
  compileMediaQueryStyle(data) {
    return this.breakPoints.list
      .filter(bp => data[bp.alias] !== void 0)
      .map(bp => this.compileBreakPoint(bp, data[bp.alias]))
      .join("\r\n");
  }


  /**
   * @protected
   * @param {BreakPoint} breakPoint
   * @param {Object} attributes
   * @return {string}
   */
  compileBreakPoint(breakPoint, attributes) {

    let before = `@media ${breakPoint.mediaQuery} {`;
    let after = `}`;
    let content = this.compileAttributes(attributes);

    return content.length ? before + content + after : '';
  }

  /**
   * @protected
   * @param attributes
   * @return {string}
   */
  compileAttributes(attributes) {
    let css = {};
    let text = '';

    if (attributes) {

      let before = `#${this.guid} {`;
      let after = `}`;
      Object.keys(attributes).forEach(name => {
        let style = this.compileAttribute(name, attributes[name], attributes);
        if (style) {
          Object.assign(css, style);
        }
      });

      let style = Object.keys(css).map(name => (`${name}: ${css[name]}`)).join(';');

      text = before + style + after;
    }

    return text;
  }

  /**
   * @protected
   * @param name
   * @param value
   * @param attributes
   * @return {*}
   */
  compileAttribute(name, value, attributes) {
    let res = null;
    let strategy = this.strategies[name];

    if (strategy) {
      res = strategy(value, attributes);
    }

    return res;
  }


}

module.exports = StyleCompiler;