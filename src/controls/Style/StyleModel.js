const StyleCompiler = require('./StyleCompiler');
const SEPARATOR = ':';

class StyleModel {

  /**
   *
   * @param {string} guid
   * @param {Object}  strategies
   */
  constructor(guid, strategies) {
    this._guid = guid;
    this.attributes = {};
    this.activeAttributes = Object.keys(strategies);
    this.styleCompiler = new StyleCompiler(guid, strategies);

  }

  get guid() {
    return this._guid;
  }

  set attributes(attributes){
    Object.keys(attributes).filter(attr => {
      let attrName = attr.split(SEPARATOR)[0];
      return this.activeAttributes.some(name => name === attrName);
    }).forEach(attr => {
      this.setAttributeValue(attr, attributes[attr]);
    });
  }

  get attributes() {
    this._attributes = this._attributes || Object.create(null);
    return this._attributes;
  }

  get css() {
    return this.styleCompiler.compile(this.attributes, SEPARATOR);
  }

  /**
   * @protected
   * @param name
   * @param value
   */
  setAttributeValue(name, value) {
    this.attributes[name] = value;
  }






}

module.exports = StyleModel;