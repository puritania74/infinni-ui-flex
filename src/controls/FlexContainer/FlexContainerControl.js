const Control = require('InfinniUI').Control;
const FlexContainerModel = require('./FlexContainerModel');
const FlexContainerView = require('./FlexContainerView');
/**
 *
 * @param parent
 * @constructor
 * @augments InfinniUI.Control
 */
function FlexContainerControl(parent) {
  Control.call(this, parent);
}

FlexContainerControl.prototype = Object.create(Control.prototype, {
  direction: {
    set: function( value ) {
      this.controlModel.setDirection(value)
    },
    get: function(  ) {
      return this.controlModel.getDirection();
    }
  },
  wrap: {
    set: function( value ) {
      this.controlModel.setWrap(value);
    },
    get: function(  ) {
      return this.controlModel.getWrap();
    }
  },
  justify: {
    set: function( value ) {
      this.controlModel.setJustify(value);
    },
    get: function(  ) {
      return this.controlModel.getJustify();
    }
  },
  align: {
    set: function( value ) {
      this.controlModel.setAlign(value);
    },
    get: function(  ) {
      return this.controlModel.getAlign();
    }
  },
  childrenAlign: {
    set: function( value ) {
      this.controlModel.setChildrenAlign(value);
    },
    get: function(  ) {
      return this.controlModel.getChildrenAlign();
    }
  },
  children: {
    set: function( value ) {
      this.controlModel.setChildren(value);
    },
    get: function(  ) {
      return this.controlModel.getChildren();
    }
  }

});

FlexContainerControl.prototype.constructor = FlexContainerControl;

FlexContainerControl.prototype.createControlModel = function () {
  return new FlexContainerModel();
};

FlexContainerControl.prototype.createControlView = function( model ) {
  return new FlexContainerView({model});
};

module.exports = FlexContainerControl;