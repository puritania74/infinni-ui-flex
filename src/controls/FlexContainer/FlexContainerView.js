const ControlView = require('InfinniUI').ControlView;
const Style = require('../Style/Style');

const Direction = require('./attributes/Direction');
const Wrap = require('./attributes/Wrap');
const Justify = require('./attributes/Justify');
const Align = require('./attributes/Align');
const ChildrenAlign = require('./attributes/ChildrenAlign');
const hasChildElementsMixin = require('./../mixins/hasChildElemensMixin');

/**
 *
 * @constructor
 * @mixes hasChildElementsMixin
 */
const FlexContainerView = ControlView.extend({

  initialize: function(  ) {
    ControlView.prototype.initialize.apply(this, Array.prototype.slice.call(arguments));
    this.initializeStyle();
  },

  render: function(  ) {
    this.prerenderingActions();

    this.updateProperties();

    this.trigger( 'render' );
    this.el.id = this.model.getId();

    this.renderStyle();
    this.renderChildren();

    this.postrenderingActions();
    return this;
  },

  renderStyle: function(  ) {
    this.$el.append(this.style.el);
  },

  /**
   * @protected
   */
  renderChildren: function(  ) {
    let children = this.model.getChildren();

    //remove previous rendered items
    this.removeChildElements();
    //render children
    this.$el.append(children.map(child => child.render() ));
    //register child elements
    this.addChildElements(children);
  },

  remove: function(  ) {
    this.removeChildElements();
    ControlView.prototype.remove.call(this);
  },

  initHandlersForProperties: function(  ) {
    ControlView.prototype.initHandlersForProperties.call(this);
    this.listenTo(this.model, 'change', this.onChangeModelHandler);

  },

  onChangeModelHandler: function( model, options ) {
    let json = model.toJSON();
    this.style.setAttributes(json);
    this.style.render();
  },

  updateProperties: function(  ) {
    ControlView.prototype.updateProperties.call(this);
    this.updateFlex();
  },

  updateFlex: function(  ) {
    this.$el.css({
      display: 'flex'
    });
  },

  initializeStyle: function() {

    this.style = new Style(this.model.getId(), {
      direction: Direction,
      wrap: Wrap,
      justify: Justify,
      align: Align,
      childrenAlign: ChildrenAlign
    });
  }

});


_.extend(FlexContainerView.prototype, hasChildElementsMixin );


module.exports = FlexContainerView;
