const ControlModel = require('InfinniUI').ControlModel;
const FlexContainerOptions = require('../../metadata/FlexContainerOptions');

/**
 *
 * @constructor
 * @augments InfinniUI.ControlModel
 */
const FlexContainerModel = ControlModel.extend({

  defaults: Object.assign({}, ControlModel.prototype.defaults, {
    direction: FlexContainerOptions.Direction.Row,
    wrap: FlexContainerOptions.Wrap.NoWrap,
    justify: FlexContainerOptions.Justify.Start,
    align: FlexContainerOptions.ContainerAlign.Stretch,
    childrenAlign: FlexContainerOptions.ChildrenAlign.Stretch
  }),

  getId: function(  ) {
    return '_flex' + this.get('guid').replace(/-/g, '');
  },

  initialize: function(  ) {
    ControlModel.prototype.initialize.apply(this, Array.prototype.slice.call(arguments));
    this.setChildren([]);
  },

  setChildren: function( children ) {
    this.set('children', children);
  },

  getChildren: function(  ) {
    return this.get('children');
  }

});

module.exports = FlexContainerModel;
