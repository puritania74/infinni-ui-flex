const FlexContainerOptions  = require('../../../metadata/FlexContainerOptions');

module.exports = function Align( align ) {

  let res = null;

  switch(align) {

    case FlexContainerOptions.ContainerAlign.SpaceBetween:
      res = 'space-between';
      break;
    case FlexContainerOptions.ContainerAlign.SpaceAround:
      res = 'space-around';
      break;
    case FlexContainerOptions.ContainerAlign.End:
      res = 'end';
      break;
    case FlexContainerOptions.ContainerAlign.Center:
      res = 'center';
      break;
    case FlexContainerOptions.ContainerAlign.Start:
      res = 'start';
      break;
    case FlexContainerOptions.ContainerAlign.Stretch:
      res = 'stretch';
      break;
  }

  return {
    'align-content': res
  };
};