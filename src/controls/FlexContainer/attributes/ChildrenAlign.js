const FlexContainerOptions  = require('../../../metadata/FlexContainerOptions');

module.exports = function( childrenAlign ) {
  let res = null;
  switch(childrenAlign) {
    case FlexContainerOptions.ChildrenAlign.Stretch:
      res = 'stretch';
      break;
    case FlexContainerOptions.ChildrenAlign.Start:
      res = 'flex-start';
      break;
    case FlexContainerOptions.ChildrenAlign.Center:
      res = 'center';
      break;
    case FlexContainerOptions.ChildrenAlign.End:
      res = 'flex-end';
      break;
    case FlexContainerOptions.ChildrenAlign.Baseline:
      res = 'baseline';
      break;
  }

  return {
    'align-items': res
  }

};