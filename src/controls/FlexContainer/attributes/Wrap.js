const FlexContainerOptions  = require('../../../metadata/FlexContainerOptions');

module.exports = function Wrap(wrap) {

  let res = null;

  switch(wrap) {
    case FlexContainerOptions.Wrap.Wrap:
      res = 'wrap';
      break;
    case FlexContainerOptions.Wrap.NoWrap:
      res = 'nowrap';
      break;
    case FlexContainerOptions.Wrap.WrapReverse:
      res = 'wrap-reverse';
      break;
  }

  return {
    'flex-wrap': res
  }
};