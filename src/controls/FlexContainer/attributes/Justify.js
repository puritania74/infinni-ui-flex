const FlexContainerOptions  = require('../../../metadata/FlexContainerOptions');

module.exports = function Justify( justify ) {
  let res = null;

  switch(justify) {
    case FlexContainerOptions.Justify.Stretch:
      res = 'stretch';
      break;
    case FlexContainerOptions.Justify.Start:
      res = 'flex-start';
      break;
    case FlexContainerOptions.Justify.Center:
      res = 'center';
      break;
    case FlexContainerOptions.Justify.End:
      res = 'flex-end';
      break;
    case FlexContainerOptions.Justify.SpaceAround:
      res = 'space-around';
      break;
    case FlexContainerOptions.Justify.SpaceBetween:
      res = 'space-between';
      break;
  }

  return {
    'justify-content': res
  };
};