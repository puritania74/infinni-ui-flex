const FlexContainerOptions  = require('../../../metadata/FlexContainerOptions');

/**
 *
 * @param {string} direction
 * @return {object}
 */
module.exports = function Direction( direction ) {
  let res = null;

  switch(direction) {
    case FlexContainerOptions.Direction.Row:
      res = 'row';
      break;
    case FlexContainerOptions.Direction.Column:
      res = 'column';
      break;
    case FlexContainerOptions.Direction.RowReverse:
      res = 'row-reverse';
      break;
    case FlexContainerOptions.Direction.ColumnReverse:
      res = 'column-reverse';
      break;
  }

  return {
    'flex-direction': res
  };
};