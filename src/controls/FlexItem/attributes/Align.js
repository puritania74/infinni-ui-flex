const FlexItemOptions  = require('../../../metadata/FlexItemOptions');

module.exports = function Align( align ) {

  let res = null;

  switch(align) {

    case FlexItemOptions.Align.Auto:
      res = 'auto';
      break;
    case FlexItemOptions.Align.Start:
      res = 'flex-start';
      break;
    case FlexItemOptions.Align.End:
      res = 'flex-end';
      break;
    case FlexItemOptions.Align.Center:
      res = 'center';
      break;
    case FlexItemOptions.Align.Baseline:
      res = 'baseline';
      break;
    case FlexItemOptions.Align.Stretch:
      res = 'stretch';
      break;
  }

  return {
    'align-self': res
  };
};