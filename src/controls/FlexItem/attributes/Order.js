/**
 *
 * @param {number} align
 */
module.exports = function Order( order ) {
  let value = null;

  if (order !== null) {
    value = +order;

    if (isNaN(value) || !isFinite(value)) {
      value = null;
    }
  }

  return value === null ? null : {order}
};