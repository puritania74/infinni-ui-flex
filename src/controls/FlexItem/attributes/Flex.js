module.exports = function Flex( value, attributes ) {

  let flex = {};
  //default 0 1 auto


  let flexGrow = parseInt(attributes['grow']);
  let flexShrink = parseInt(attributes['shrink']);
  let flexBasis = attributes['basis'] || '';


  flexBasis = flexBasis.replace(/[^a-z0-9%]/i, '');

  if (!isNaN(flexGrow) || isFinite(flexGrow) ) {
    flex['flex-grow'] = flexGrow ;
  }

  if (!isNaN(flexShrink) || isFinite(flexShrink)) {
    flex['flex-shrink'] = flexShrink
  }

  if (flexBasis) {
    flex['flex-basis'] = flexBasis;
  }

  return Object.keys(flex).length ? flex : null;
};