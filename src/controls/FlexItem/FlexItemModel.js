const ControlModel = require('InfinniUI').ControlModel;
const FlexItemOptions = require('../../metadata/FlexItemOptions');

/**
 *
 * @constructor
 * @augments InfinniUI.ControlModel
 */
const FlexItemModel = ControlModel.extend({

  defaults: Object.assign({}, ControlModel.prototype.defaults, {
    align: FlexItemOptions.Align.Auto,
    grow: 0,
    shrink: 1,
    basis: 'auto'
  }),

  getId: function(  ) {
    return '_flexitem' + this.get('guid').replace(/-/g, '');
  },

  getContent: function(  ) {
    return this.get('content');
  }

});

module.exports = FlexItemModel;
