const ControlView = require('InfinniUI').ControlView;
const Style = require('../Style/Style');

const Align = require('./attributes/Align');
const Flex = require('./attributes/Flex');
const Order = require('./attributes/Order');

const hasChildElementsMixin = require('./../mixins/hasChildElemensMixin');

const FlexItemView = ControlView.extend({

  initialize: function(  ) {
    ControlView.prototype.initialize.apply(this, Array.prototype.slice.call(arguments));
    this.initializeStyle();
  },

  render: function(  ) {
    this.prerenderingActions();
    this.updateProperties();

    this.trigger( 'render' );
    this.el.id = this.model.getId();

    this.renderStyle();
    this.renderContent();

    this.postrenderingActions();
    return this;
  },

  renderStyle: function(  ) {
    this.$el.append(this.style.el);
  },

  renderContent: function(  ) {
    const contentElementFactory = this.model.getContent();
    const contentElement = contentElementFactory();

    this.removeChildElements();
    if (contentElement) {
      this.addChildElement(contentElement);
      this.$el.append(contentElement.render());

    }

  },

  initHandlersForProperties: function(  ) {
    ControlView.prototype.initHandlersForProperties.call(this);
    this.listenTo(this.model, 'change', this.onChangeModelHandler);
  },


  onChangeModelHandler: function( model, options ) {
    let json = model.toJSON();
    this.style.setAttributes(json);
    this.style.render();
  },

  /**
   * @protected
   */
  initializeStyle: function() {

    this.style = new Style(this.model.getId(), {
      align: Align,
      order: Order,
      grow: Flex,
      shrink: Flex,
      basis: Flex
    });
  },


});

_.extend(FlexItemView.prototype, hasChildElementsMixin );

module.exports = FlexItemView;
