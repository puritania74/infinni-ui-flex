const Control = require( 'InfinniUI' ).Control;
const FlexItemModel = require( './FlexItemModel' );
const FlexItemView = require( './FlexItemView' );

class FlexItemControl extends Control {

  constructor(parent) {
    super(parent);
  }

  createControlModel() {
    return new FlexItemModel();
  }

  createControlView ( model ) {
    return new FlexItemView( { model } );
  }
}

module.exports = FlexItemControl;