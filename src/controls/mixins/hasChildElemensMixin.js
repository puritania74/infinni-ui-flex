const CHILD_ELEMENTS_ATTRIBUTE = '_childElements';

module.exports = {

  getChildElements: function(  ) {
    return this[CHILD_ELEMENTS_ATTRIBUTE] = this[CHILD_ELEMENTS_ATTRIBUTE] || [];
  },

  addChildElement: function( child ) {
    let childElements = this.getChildElements();
    if (childElements.indexOf(child) === -1) {
      childElements.push(child);
    }
  },

  addChildElements: function( children ) {
    children.forEach(child => this.addChildElement(child));
  },

  removeChildElements: function(  ) {
    let childElements = this.getChildElements();
    childElements.forEach(child => child.remove());
    childElements.length = 0;
  }

};