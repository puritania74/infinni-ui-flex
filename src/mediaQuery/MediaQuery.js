class MediaQuery{

  /**
   *
   * @param {Array} conditions
   * @return {string}
   */
  compile(conditions) {
    return conditions
      .map(condition => this.compileCondition(condition))
      .join(' and ');
  }

  /**
   *
   * @param condition
   * @return {string}
   */
  compileCondition(condition) {
    let res = '';

    if (typeof condition === 'string') {
      res = condition;
    } else {
      //Object key/value
      let key = Object.keys(condition).pop();
      let value = condition[key];
      res = `(${key}: ${value})`;
    }

    return res;
  }



}

module.exports = MediaQuery;