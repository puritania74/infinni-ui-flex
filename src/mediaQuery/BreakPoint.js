const MediaQuery = require('./MediaQuery');
class BreakPoint {

  /**
   *
   * @return {string}
   */
  get alias() {
    return this._alias;
  }

  /**
   *
   * @return {string}
   */
  get mediaQuery() {
    return this._mediaQuery
  }

  constructor(config) {
    let alias = Object.keys(config)[0];
    let conditions = config[alias];
    let mediaQuery = new MediaQuery();
    this._alias = alias;
    this._mediaQuery = mediaQuery.compile(conditions);
  }

}

module.exports = BreakPoint;