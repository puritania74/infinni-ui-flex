const BreakPoint = require('./BreakPoint');

class BreakPoints {

  /**
   *
   * @param {Array<Object>} breakpoints configuration break points
   */
  constructor(breakpoints) {

    this.init(breakpoints || []);
  }

  init(breakpoints) {
    this._list = breakpoints.map(item => new BreakPoint(item));
  }

  /**
   *
   * @return {Array<BreakPoint>}
   */
  get list() {
    return this._list;
  }

  get aliasList() {
    if (!this._aliasList) {
      this._aliasList = this.list.map(breakPoint => breakPoint.alias);
    }

    return this._aliasList;
  }
}


module.exports = BreakPoints;