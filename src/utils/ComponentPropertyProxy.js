function ComponentPropertyProxy( name ) {

  return {
    set: function( value ) {
      this.control[name] = value;
    },

    get: function(  ) {
      return this.control[name];
    }
  }

}


module.exports = ComponentPropertyProxy;