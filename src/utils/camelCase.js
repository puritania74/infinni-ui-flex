/**
 *
 * @param {string} text
 * @return {string}
 */
module.exports = function( text ) {
  return text.substr(0, 1).toLowerCase() + text.substr(1);
};