const camelCase = require('./camelCase');
const PROPERTY_SEPARATOR = require('../metadata/PropertySeparator');

/**
 *
 * @param {string} name
 * @param {string} [size]
 * @return {string}
 */
function normalizePropertyName( name, size ) {
  let propName = camelCase(name);

  if (size !== void 0) {
    propName = propName.concat(PROPERTY_SEPARATOR, size);
  }

  return propName;
}

module.exports = normalizePropertyName;