import babel from 'rollup-plugin-babel';
import commonjs from 'rollup-plugin-commonjs';
import resolve from 'rollup-plugin-node-resolve';
import globals from 'rollup-plugin-node-globals';

export default {
  entry: 'src/main.js',
  dest: 'dist/infinni-flex.js',
  format: 'iife',
  moduleName: 'InfinniFlex',
  external: ['InfinniUI'],
  globals: {
    "InfinniUI": "InfinniUI"
  },
  plugins: [
    globals({
      'InfinniUI': 'InfinniUI'
    }),
    resolve({
      preferBuiltins: false,
      browser: true
    }),
    commonjs(),
    babel({
      exclude: 'node_modules/**'
    })
  ]
};