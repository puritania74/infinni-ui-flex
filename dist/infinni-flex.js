var InfinniFlex = (function (require$$0) {
'use strict';

require$$0 = 'default' in require$$0 ? require$$0['default'] : require$$0;

/**
 * @description transform to align-items
 * @enum {string}
 */
var ChildrenAlign = {
  Start: 'Start',
  End: 'End',
  Center: 'Center',
  Baseline: 'Baseline',
  Stretch: 'Stretch'
};

/**
 * @description transform to align-content
 * @enum {string}
 */
var ContainerAlign = {
  Start: 'Start',
  End: 'End',
  Center: 'Center',
  SpaceBetween: 'SpaceBetween',
  SpaceAround: 'SpaceAround',
  Stretch: 'Stretch'
};

/**
 * @description transform to justify-content
 * @enum {string}
 */
var Justify = {
  Start: 'Start',
  End: 'End',
  Center: 'Center',
  SpaceBetween: 'SpaceBetween',
  SpaceAround: 'SpaceAround',
  Stretch: 'Stretch'
};

/**
 * @description transform to flex-direction
 * @enum {string}
 */
var Direction = {
  Row: 'Row',
  RowReverse: 'RowReverse',
  Column: 'Column',
  ColumnReverse: 'ColumnReverse'
};

/**
 * @description transform to flex-wrap
 * @enum {string}
 */
var Wrap = {
  NoWrap: 'NoWrap',
  Wrap: 'Wrap',
  WrapReverse: 'WrapReverse'
};

var FlexContainerOptions = {
  Direction: Direction,
  Wrap: Wrap,
  Justify: Justify,
  ContainerAlign: ContainerAlign,
  ChildrenAlign: ChildrenAlign
};

var classCallCheck = function (instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
};

var createClass = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }

  return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);
    if (staticProps) defineProperties(Constructor, staticProps);
    return Constructor;
  };
}();







var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];

    for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }

  return target;
};

var get = function get(object, property, receiver) {
  if (object === null) object = Function.prototype;
  var desc = Object.getOwnPropertyDescriptor(object, property);

  if (desc === undefined) {
    var parent = Object.getPrototypeOf(object);

    if (parent === null) {
      return undefined;
    } else {
      return get(parent, property, receiver);
    }
  } else if ("value" in desc) {
    return desc.value;
  } else {
    var getter = desc.get;

    if (getter === undefined) {
      return undefined;
    }

    return getter.call(receiver);
  }
};

var inherits = function (subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      enumerable: false,
      writable: true,
      configurable: true
    }
  });
  if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
};











var possibleConstructorReturn = function (self, call) {
  if (!self) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return call && (typeof call === "object" || typeof call === "function") ? call : self;
};





var slicedToArray = function () {
  function sliceIterator(arr, i) {
    var _arr = [];
    var _n = true;
    var _d = false;
    var _e = undefined;

    try {
      for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) {
        _arr.push(_s.value);

        if (i && _arr.length === i) break;
      }
    } catch (err) {
      _d = true;
      _e = err;
    } finally {
      try {
        if (!_n && _i["return"]) _i["return"]();
      } finally {
        if (_d) throw _e;
      }
    }

    return _arr;
  }

  return function (arr, i) {
    if (Array.isArray(arr)) {
      return arr;
    } else if (Symbol.iterator in Object(arr)) {
      return sliceIterator(arr, i);
    } else {
      throw new TypeError("Invalid attempt to destructure non-iterable instance");
    }
  };
}();

var ControlModel = require$$0.ControlModel;

/**
 *
 * @constructor
 * @augments InfinniUI.ControlModel
 */
var FlexContainerModel = ControlModel.extend({

  defaults: _extends({}, ControlModel.prototype.defaults, {
    direction: FlexContainerOptions.Direction.Row,
    wrap: FlexContainerOptions.Wrap.NoWrap,
    justify: FlexContainerOptions.Justify.Start,
    align: FlexContainerOptions.ContainerAlign.Stretch,
    childrenAlign: FlexContainerOptions.ChildrenAlign.Stretch
  }),

  getId: function getId() {
    return '_flex' + this.get('guid').replace(/-/g, '');
  },

  initialize: function initialize() {
    ControlModel.prototype.initialize.apply(this, Array.prototype.slice.call(arguments));
    this.setChildren([]);
  },

  setChildren: function setChildren(children) {
    this.set('children', children);
  },

  getChildren: function getChildren() {
    return this.get('children');
  }

});

var FlexContainerModel_1 = FlexContainerModel;

var Config = function () {
  function Config() {
    classCallCheck(this, Config);

    this.data = Object.create(null);
  }

  createClass(Config, [{
    key: "set",
    value: function set$$1(name, value) {
      this.data[name] = value;
    }
  }, {
    key: "get",
    value: function get$$1(name, defaultValue) {
      return name in this.data ? this.data[name] : defaultValue;
    }
  }]);
  return Config;
}();

var Config_1 = Config;

var breakPoints = [{
  'xs': ['screen']
}, {
  'sm': ['screen', { 'min-width': '600px' }]
}, {
  'md': ['screen', { 'min-width': '960px' }]
}, {
  'lg': ['screen', { 'min-width': '1280px' }]
}, {
  'xl': ['screen', { 'min-width': '1920px' }]
}];

var config = new Config_1();
config.set('breakPoints', breakPoints);

var index = config;

var MediaQuery = function () {
  function MediaQuery() {
    classCallCheck(this, MediaQuery);
  }

  createClass(MediaQuery, [{
    key: 'compile',


    /**
     *
     * @param {Array} conditions
     * @return {string}
     */
    value: function compile(conditions) {
      var _this = this;

      return conditions.map(function (condition) {
        return _this.compileCondition(condition);
      }).join(' and ');
    }

    /**
     *
     * @param condition
     * @return {string}
     */

  }, {
    key: 'compileCondition',
    value: function compileCondition(condition) {
      var res = '';

      if (typeof condition === 'string') {
        res = condition;
      } else {
        //Object key/value
        var key = Object.keys(condition).pop();
        var value = condition[key];
        res = '(' + key + ': ' + value + ')';
      }

      return res;
    }
  }]);
  return MediaQuery;
}();

var MediaQuery_1 = MediaQuery;

var BreakPoint = function () {
  createClass(BreakPoint, [{
    key: 'alias',


    /**
     *
     * @return {string}
     */
    get: function get$$1() {
      return this._alias;
    }

    /**
     *
     * @return {string}
     */

  }, {
    key: 'mediaQuery',
    get: function get$$1() {
      return this._mediaQuery;
    }
  }]);

  function BreakPoint(config) {
    classCallCheck(this, BreakPoint);

    var alias = Object.keys(config)[0];
    var conditions = config[alias];
    var mediaQuery = new MediaQuery_1();
    this._alias = alias;
    this._mediaQuery = mediaQuery.compile(conditions);
  }

  return BreakPoint;
}();

var BreakPoint_1 = BreakPoint;

var BreakPoints = function () {

  /**
   *
   * @param {Array<Object>} breakpoints configuration break points
   */
  function BreakPoints(breakpoints) {
    classCallCheck(this, BreakPoints);


    this.init(breakpoints || []);
  }

  createClass(BreakPoints, [{
    key: 'init',
    value: function init(breakpoints) {
      this._list = breakpoints.map(function (item) {
        return new BreakPoint_1(item);
      });
    }

    /**
     *
     * @return {Array<BreakPoint>}
     */

  }, {
    key: 'list',
    get: function get$$1() {
      return this._list;
    }
  }, {
    key: 'aliasList',
    get: function get$$1() {
      if (!this._aliasList) {
        this._aliasList = this.list.map(function (breakPoint) {
          return breakPoint.alias;
        });
      }

      return this._aliasList;
    }
  }]);
  return BreakPoints;
}();

var BreakPoints_1 = BreakPoints;

var COMMON_STYLE = 'COMMON';

var StyleCompiler = function () {
  function StyleCompiler(guid, attributeStrategies) {
    classCallCheck(this, StyleCompiler);

    this.guid = guid;
    this.strategies = {};
    this.registerAttributeStrategies(attributeStrategies);
    this.breakPoints = new BreakPoints_1(index.get('breakPoints'));
  }

  createClass(StyleCompiler, [{
    key: 'compile',
    value: function compile(attributes, separator) {
      var data = {};

      Object.keys(attributes).forEach(function (name) {
        var _name$split = name.split(separator),
            _name$split2 = slicedToArray(_name$split, 2),
            prop = _name$split2[0],
            _name$split2$ = _name$split2[1],
            size = _name$split2$ === undefined ? COMMON_STYLE : _name$split2$;

        var style = data[size] = data[size] || {};
        style[prop] = attributes[name];
      });

      var css = this.compileCommonStyle(data);
      css += this.compileMediaQueryStyle(data);

      return css;
    }

    /**
     * @protected
     * @param strategies
     */

  }, {
    key: 'registerAttributeStrategies',
    value: function registerAttributeStrategies(strategies) {
      var _this = this;

      Object.keys(strategies).forEach(function (name) {
        return _this.strategies[name] = strategies[name];
      });
    }

    /**
     * @protected
     * @param {Object} data
     * @return {string}
     */

  }, {
    key: 'compileCommonStyle',
    value: function compileCommonStyle(data) {
      return this.compileAttributes(data[COMMON_STYLE]);
    }

    /**
     * @protected
     * @param data
     * @return {string}
     */

  }, {
    key: 'compileMediaQueryStyle',
    value: function compileMediaQueryStyle(data) {
      var _this2 = this;

      return this.breakPoints.list.filter(function (bp) {
        return data[bp.alias] !== void 0;
      }).map(function (bp) {
        return _this2.compileBreakPoint(bp, data[bp.alias]);
      }).join("\r\n");
    }

    /**
     * @protected
     * @param {BreakPoint} breakPoint
     * @param {Object} attributes
     * @return {string}
     */

  }, {
    key: 'compileBreakPoint',
    value: function compileBreakPoint(breakPoint, attributes) {

      var before = '@media ' + breakPoint.mediaQuery + ' {';
      var after = '}';
      var content = this.compileAttributes(attributes);

      return content.length ? before + content + after : '';
    }

    /**
     * @protected
     * @param attributes
     * @return {string}
     */

  }, {
    key: 'compileAttributes',
    value: function compileAttributes(attributes) {
      var _this3 = this;

      var css = {};
      var text = '';

      if (attributes) {

        var before = '#' + this.guid + ' {';
        var after = '}';
        Object.keys(attributes).forEach(function (name) {
          var style = _this3.compileAttribute(name, attributes[name], attributes);
          if (style) {
            _extends(css, style);
          }
        });

        var style = Object.keys(css).map(function (name) {
          return name + ': ' + css[name];
        }).join(';');

        text = before + style + after;
      }

      return text;
    }

    /**
     * @protected
     * @param name
     * @param value
     * @param attributes
     * @return {*}
     */

  }, {
    key: 'compileAttribute',
    value: function compileAttribute(name, value, attributes) {
      var res = null;
      var strategy = this.strategies[name];

      if (strategy) {
        res = strategy(value, attributes);
      }

      return res;
    }
  }]);
  return StyleCompiler;
}();

var StyleCompiler_1 = StyleCompiler;

var SEPARATOR = ':';

var StyleModel = function () {

  /**
   *
   * @param {string} guid
   * @param {Object}  strategies
   */
  function StyleModel(guid, strategies) {
    classCallCheck(this, StyleModel);

    this._guid = guid;
    this.attributes = {};
    this.activeAttributes = Object.keys(strategies);
    this.styleCompiler = new StyleCompiler_1(guid, strategies);
  }

  createClass(StyleModel, [{
    key: 'setAttributeValue',


    /**
     * @protected
     * @param name
     * @param value
     */
    value: function setAttributeValue(name, value) {
      this.attributes[name] = value;
    }
  }, {
    key: 'guid',
    get: function get$$1() {
      return this._guid;
    }
  }, {
    key: 'attributes',
    set: function set$$1(attributes) {
      var _this = this;

      Object.keys(attributes).filter(function (attr) {
        var attrName = attr.split(SEPARATOR)[0];
        return _this.activeAttributes.some(function (name) {
          return name === attrName;
        });
      }).forEach(function (attr) {
        _this.setAttributeValue(attr, attributes[attr]);
      });
    },
    get: function get$$1() {
      this._attributes = this._attributes || Object.create(null);
      return this._attributes;
    }
  }, {
    key: 'css',
    get: function get$$1() {
      return this.styleCompiler.compile(this.attributes, SEPARATOR);
    }
  }]);
  return StyleModel;
}();

var StyleModel_1 = StyleModel;

var Style = function () {
  createClass(Style, [{
    key: 'el',
    get: function get$$1() {
      if (!this._el) {
        this._el = this.createStyleElement();
      }
      return this._el;
    }

    /**
     *
     * @param {string} guid
     * @param {Array<Function>} attributeStrategies
     */

  }]);

  function Style(guid, attributeStrategies) {
    classCallCheck(this, Style);

    this.model = new StyleModel_1(guid, attributeStrategies);
  }

  createClass(Style, [{
    key: 'setAttributes',
    value: function setAttributes(attributes) {
      this.model.attributes = attributes;
    }
  }, {
    key: 'render',
    value: function render() {
      var el = this.el;
      //remove all child nodes
      while (el.firstChild) {
        el.removeChild(el.firstChild);
      }

      var styleText = document.createTextNode(this.model.css);
      el.appendChild(styleText);
    }

    /**
     * @protected
     * @return {Element}
     */

  }, {
    key: 'createStyleElement',
    value: function createStyleElement() {
      var node = document.createElement('style');
      node.type = "text/css";
      return node;
    }
  }]);
  return Style;
}();

var Style_1 = Style;

/**
 *
 * @param {string} direction
 * @return {object}
 */
var Direction$1 = function Direction(direction) {
  var res = null;

  switch (direction) {
    case FlexContainerOptions.Direction.Row:
      res = 'row';
      break;
    case FlexContainerOptions.Direction.Column:
      res = 'column';
      break;
    case FlexContainerOptions.Direction.RowReverse:
      res = 'row-reverse';
      break;
    case FlexContainerOptions.Direction.ColumnReverse:
      res = 'column-reverse';
      break;
  }

  return {
    'flex-direction': res
  };
};

var Wrap$1 = function Wrap(wrap) {

  var res = null;

  switch (wrap) {
    case FlexContainerOptions.Wrap.Wrap:
      res = 'wrap';
      break;
    case FlexContainerOptions.Wrap.NoWrap:
      res = 'nowrap';
      break;
    case FlexContainerOptions.Wrap.WrapReverse:
      res = 'wrap-reverse';
      break;
  }

  return {
    'flex-wrap': res
  };
};

var Justify$1 = function Justify(justify) {
  var res = null;

  switch (justify) {
    case FlexContainerOptions.Justify.Stretch:
      res = 'stretch';
      break;
    case FlexContainerOptions.Justify.Start:
      res = 'flex-start';
      break;
    case FlexContainerOptions.Justify.Center:
      res = 'center';
      break;
    case FlexContainerOptions.Justify.End:
      res = 'flex-end';
      break;
    case FlexContainerOptions.Justify.SpaceAround:
      res = 'space-around';
      break;
    case FlexContainerOptions.Justify.SpaceBetween:
      res = 'space-between';
      break;
  }

  return {
    'justify-content': res
  };
};

var Align = function Align(align) {

  var res = null;

  switch (align) {

    case FlexContainerOptions.ContainerAlign.SpaceBetween:
      res = 'space-between';
      break;
    case FlexContainerOptions.ContainerAlign.SpaceAround:
      res = 'space-around';
      break;
    case FlexContainerOptions.ContainerAlign.End:
      res = 'end';
      break;
    case FlexContainerOptions.ContainerAlign.Center:
      res = 'center';
      break;
    case FlexContainerOptions.ContainerAlign.Start:
      res = 'start';
      break;
    case FlexContainerOptions.ContainerAlign.Stretch:
      res = 'stretch';
      break;
  }

  return {
    'align-content': res
  };
};

var ChildrenAlign$1 = function ChildrenAlign(childrenAlign) {
  var res = null;
  switch (childrenAlign) {
    case FlexContainerOptions.ChildrenAlign.Stretch:
      res = 'stretch';
      break;
    case FlexContainerOptions.ChildrenAlign.Start:
      res = 'flex-start';
      break;
    case FlexContainerOptions.ChildrenAlign.Center:
      res = 'center';
      break;
    case FlexContainerOptions.ChildrenAlign.End:
      res = 'flex-end';
      break;
    case FlexContainerOptions.ChildrenAlign.Baseline:
      res = 'baseline';
      break;
  }

  return {
    'align-items': res
  };
};

var CHILD_ELEMENTS_ATTRIBUTE = '_childElements';

var hasChildElemensMixin = {

  getChildElements: function getChildElements() {
    return this[CHILD_ELEMENTS_ATTRIBUTE] = this[CHILD_ELEMENTS_ATTRIBUTE] || [];
  },

  addChildElement: function addChildElement(child) {
    var childElements = this.getChildElements();
    if (childElements.indexOf(child) === -1) {
      childElements.push(child);
    }
  },

  addChildElements: function addChildElements(children) {
    var _this = this;

    children.forEach(function (child) {
      return _this.addChildElement(child);
    });
  },

  removeChildElements: function removeChildElements() {
    var childElements = this.getChildElements();
    childElements.forEach(function (child) {
      return child.remove();
    });
    childElements.length = 0;
  }

};

var ControlView = require$$0.ControlView;

/**
 *
 * @constructor
 * @mixes hasChildElementsMixin
 */
var FlexContainerView = ControlView.extend({

  initialize: function initialize() {
    ControlView.prototype.initialize.apply(this, Array.prototype.slice.call(arguments));
    this.initializeStyle();
  },

  render: function render() {
    this.prerenderingActions();

    this.updateProperties();

    this.trigger('render');
    this.el.id = this.model.getId();

    this.renderStyle();
    this.renderChildren();

    this.postrenderingActions();
    return this;
  },

  renderStyle: function renderStyle() {
    this.$el.append(this.style.el);
  },

  /**
   * @protected
   */
  renderChildren: function renderChildren() {
    var children = this.model.getChildren();

    //remove previous rendered items
    this.removeChildElements();
    //render children
    this.$el.append(children.map(function (child) {
      return child.render();
    }));
    //register child elements
    this.addChildElements(children);
  },

  remove: function remove() {
    this.removeChildElements();
    ControlView.prototype.remove.call(this);
  },

  initHandlersForProperties: function initHandlersForProperties() {
    ControlView.prototype.initHandlersForProperties.call(this);
    this.listenTo(this.model, 'change', this.onChangeModelHandler);
  },

  onChangeModelHandler: function onChangeModelHandler(model, options) {
    var json = model.toJSON();
    this.style.setAttributes(json);
    this.style.render();
  },

  updateProperties: function updateProperties() {
    ControlView.prototype.updateProperties.call(this);
    this.updateFlex();
  },

  updateFlex: function updateFlex() {
    this.$el.css({
      display: 'flex'
    });
  },

  initializeStyle: function initializeStyle() {

    this.style = new Style_1(this.model.getId(), {
      direction: Direction$1,
      wrap: Wrap$1,
      justify: Justify$1,
      align: Align,
      childrenAlign: ChildrenAlign$1
    });
  }

});

_.extend(FlexContainerView.prototype, hasChildElemensMixin);

var FlexContainerView_1 = FlexContainerView;

var Control = require$$0.Control;

/**
 *
 * @param parent
 * @constructor
 * @augments InfinniUI.Control
 */
function FlexContainerControl(parent) {
  Control.call(this, parent);
}

FlexContainerControl.prototype = Object.create(Control.prototype, {
  direction: {
    set: function set(value) {
      this.controlModel.setDirection(value);
    },
    get: function get() {
      return this.controlModel.getDirection();
    }
  },
  wrap: {
    set: function set(value) {
      this.controlModel.setWrap(value);
    },
    get: function get() {
      return this.controlModel.getWrap();
    }
  },
  justify: {
    set: function set(value) {
      this.controlModel.setJustify(value);
    },
    get: function get() {
      return this.controlModel.getJustify();
    }
  },
  align: {
    set: function set(value) {
      this.controlModel.setAlign(value);
    },
    get: function get() {
      return this.controlModel.getAlign();
    }
  },
  childrenAlign: {
    set: function set(value) {
      this.controlModel.setChildrenAlign(value);
    },
    get: function get() {
      return this.controlModel.getChildrenAlign();
    }
  },
  children: {
    set: function set(value) {
      this.controlModel.setChildren(value);
    },
    get: function get() {
      return this.controlModel.getChildren();
    }
  }

});

FlexContainerControl.prototype.constructor = FlexContainerControl;

FlexContainerControl.prototype.createControlModel = function () {
  return new FlexContainerModel_1();
};

FlexContainerControl.prototype.createControlView = function (model) {
  return new FlexContainerView_1({ model: model });
};

var FlexContainerControl_1 = FlexContainerControl;

var Element = require$$0.Element;

var FlexContainer = function (_Element) {
  inherits(FlexContainer, _Element);

  function FlexContainer(parent) {
    classCallCheck(this, FlexContainer);
    return possibleConstructorReturn(this, (FlexContainer.__proto__ || Object.getPrototypeOf(FlexContainer)).call(this, parent));
  }

  createClass(FlexContainer, [{
    key: 'createControl',
    value: function createControl(parent) {
      return new FlexContainerControl_1(parent);
    }

    /**
     *
     * @param {string} name
     * @param {*} value
     */

  }, {
    key: 'setContainerProperty',
    value: function setContainerProperty(name, value) {
      this.control.set(name, value);
    }

    /**
     *
     * @param {string} name
     * @return {*}
     */

  }, {
    key: 'getContainerProperty',
    value: function getContainerProperty(name) {
      return this.control.get(name);
    }
  }, {
    key: 'setChildren',
    value: function setChildren(value) {
      this.control.set('children', value);
    }
  }, {
    key: 'getChildren',
    value: function getChildren() {
      return this.control.get('children');
    }
  }]);
  return FlexContainer;
}(Element);

var FlexContainer_1 = FlexContainer;

var ElementBuilder = require$$0.ElementBuilder;

var FlexContainerBuilder = function (_ElementBuilder) {
  inherits(FlexContainerBuilder, _ElementBuilder);

  function FlexContainerBuilder() {
    classCallCheck(this, FlexContainerBuilder);
    return possibleConstructorReturn(this, (FlexContainerBuilder.__proto__ || Object.getPrototypeOf(FlexContainerBuilder)).call(this));
  }

  createClass(FlexContainerBuilder, [{
    key: 'createElement',
    value: function createElement(params) {
      return new FlexContainer_1(params.parent);
    }
  }, {
    key: 'applyMetadata',
    value: function applyMetadata(params) {
      get(FlexContainerBuilder.prototype.__proto__ || Object.getPrototypeOf(FlexContainerBuilder.prototype), 'applyMetadata', this).call(this, params);
      this.applyProperties(params);
      this.applyChildren(params);
    }

    /**
     * @protected
     */

  }, {
    key: 'applyProperties',
    value: function applyProperties(params) {
      var _this2 = this;

      var element = params.element;
      var metadata = params.metadata;

      var attributes = 'Direction,Wrap,Justify,Align,ChildrenAlign'.split(',');

      Object.keys(metadata).filter(function (name) {
        return attributes.some(function (prop) {
          return name.indexOf(prop) === 0;
        });
      }).forEach(function (name) {
        return element.setContainerProperty(_this2.lowerFirstSymbol(name), metadata[name]);
      });

      return this;
    }

    /**
     * @protected
     * @param params
     */

  }, {
    key: 'applyChildren',
    value: function applyChildren(params) {
      var metadata = params.metadata;
      var element = params.element;
      var children = metadata['Children'] || [];

      var instances = children.map(function (childrenMetadata) {
        return params.builder.build(childrenMetadata, {
          parent: element,
          parentView: params.parentView,
          basePathOfProperty: params.basePathOfProperty
        });
      });

      element.setChildren(instances);
    }
  }]);
  return FlexContainerBuilder;
}(ElementBuilder);

var FlexContainerBuilder_1 = FlexContainerBuilder;

/**
 * @description transform to align-self
 * @enum {string}
 */
var Align$3 = {
  Auto: 'Auto',
  Start: 'Start',
  End: 'End',
  Center: 'Center',
  Baseline: 'Baseline',
  Stretch: 'Stretch'
};

var FlexItemOptions = { Align: Align$3 };

var ControlModel$1 = require$$0.ControlModel;

/**
 *
 * @constructor
 * @augments InfinniUI.ControlModel
 */
var FlexItemModel = ControlModel$1.extend({

  defaults: _extends({}, ControlModel$1.prototype.defaults, {
    align: FlexItemOptions.Align.Auto,
    grow: 0,
    shrink: 1,
    basis: 'auto'
  }),

  getId: function getId() {
    return '_flexitem' + this.get('guid').replace(/-/g, '');
  },

  getContent: function getContent() {
    return this.get('content');
  }

});

var FlexItemModel_1 = FlexItemModel;

var Align$4 = function Align(align) {

  var res = null;

  switch (align) {

    case FlexItemOptions.Align.Auto:
      res = 'auto';
      break;
    case FlexItemOptions.Align.Start:
      res = 'flex-start';
      break;
    case FlexItemOptions.Align.End:
      res = 'flex-end';
      break;
    case FlexItemOptions.Align.Center:
      res = 'center';
      break;
    case FlexItemOptions.Align.Baseline:
      res = 'baseline';
      break;
    case FlexItemOptions.Align.Stretch:
      res = 'stretch';
      break;
  }

  return {
    'align-self': res
  };
};

var Flex = function Flex(value, attributes) {

  var flex = {};
  //default 0 1 auto


  var flexGrow = parseInt(attributes['grow']);
  var flexShrink = parseInt(attributes['shrink']);
  var flexBasis = attributes['basis'] || '';

  flexBasis = flexBasis.replace(/[^a-z0-9%]/i, '');

  if (!isNaN(flexGrow) || isFinite(flexGrow)) {
    flex['flex-grow'] = flexGrow;
  }

  if (!isNaN(flexShrink) || isFinite(flexShrink)) {
    flex['flex-shrink'] = flexShrink;
  }

  if (flexBasis) {
    flex['flex-basis'] = flexBasis;
  }

  return Object.keys(flex).length ? flex : null;
};

/**
 *
 * @param {number} align
 */
var Order = function Order(order) {
  var value = null;

  if (order !== null) {
    value = +order;

    if (isNaN(value) || !isFinite(value)) {
      value = null;
    }
  }

  return value === null ? null : { order: order };
};

var ControlView$1 = require$$0.ControlView;

var FlexItemView = ControlView$1.extend({

  initialize: function initialize() {
    ControlView$1.prototype.initialize.apply(this, Array.prototype.slice.call(arguments));
    this.initializeStyle();
  },

  render: function render() {
    this.prerenderingActions();
    this.updateProperties();

    this.trigger('render');
    this.el.id = this.model.getId();

    this.renderStyle();
    this.renderContent();

    this.postrenderingActions();
    return this;
  },

  renderStyle: function renderStyle() {
    this.$el.append(this.style.el);
  },

  renderContent: function renderContent() {
    var contentElementFactory = this.model.getContent();
    var contentElement = contentElementFactory();

    this.removeChildElements();
    if (contentElement) {
      this.addChildElement(contentElement);
      this.$el.append(contentElement.render());
    }
  },

  initHandlersForProperties: function initHandlersForProperties() {
    ControlView$1.prototype.initHandlersForProperties.call(this);
    this.listenTo(this.model, 'change', this.onChangeModelHandler);
  },

  onChangeModelHandler: function onChangeModelHandler(model, options) {
    var json = model.toJSON();
    this.style.setAttributes(json);
    this.style.render();
  },

  /**
   * @protected
   */
  initializeStyle: function initializeStyle() {

    this.style = new Style_1(this.model.getId(), {
      align: Align$4,
      order: Order,
      grow: Flex,
      shrink: Flex,
      basis: Flex
    });
  }

});

_.extend(FlexItemView.prototype, hasChildElemensMixin);

var FlexItemView_1 = FlexItemView;

var Control$1 = require$$0.Control;

var FlexItemControl = function (_Control) {
  inherits(FlexItemControl, _Control);

  function FlexItemControl(parent) {
    classCallCheck(this, FlexItemControl);
    return possibleConstructorReturn(this, (FlexItemControl.__proto__ || Object.getPrototypeOf(FlexItemControl)).call(this, parent));
  }

  createClass(FlexItemControl, [{
    key: 'createControlModel',
    value: function createControlModel() {
      return new FlexItemModel_1();
    }
  }, {
    key: 'createControlView',
    value: function createControlView(model) {
      return new FlexItemView_1({ model: model });
    }
  }]);
  return FlexItemControl;
}(Control$1);

var FlexItemControl_1 = FlexItemControl;

var Element$1 = require$$0.Element;

var FlexItem = function (_Element) {
  inherits(FlexItem, _Element);

  function FlexItem(parent) {
    classCallCheck(this, FlexItem);
    return possibleConstructorReturn(this, (FlexItem.__proto__ || Object.getPrototypeOf(FlexItem)).call(this, parent));
  }

  createClass(FlexItem, [{
    key: 'createControl',
    value: function createControl(parent) {
      return new FlexItemControl_1(parent);
    }

    /**
     *
     * @param {string} name
     * @param {*} value
     */

  }, {
    key: 'setItemProperty',
    value: function setItemProperty(name, value) {
      this.control.set(name, value);
    }

    /**
     *
     * @param {string} name
     * @return {*}
     */

  }, {
    key: 'getItemProperty',
    value: function getItemProperty(name) {
      return this.control.get(name);
    }

    /**
     *
     * @param {Function} value
     */

  }, {
    key: 'setContent',
    value: function setContent(value) {
      this.control.set('content', value);
    }

    /**
     * @return {Function}
     */

  }, {
    key: 'getContent',
    value: function getContent() {
      return this.control.get('content');
    }
  }]);
  return FlexItem;
}(Element$1);

var FlexItem_1 = FlexItem;

var ElementBuilder$1 = require$$0.ElementBuilder;

/**
 * @class
 */

var FlexItemBuilder = function (_ElementBuilder) {
  inherits(FlexItemBuilder, _ElementBuilder);

  function FlexItemBuilder() {
    classCallCheck(this, FlexItemBuilder);
    return possibleConstructorReturn(this, (FlexItemBuilder.__proto__ || Object.getPrototypeOf(FlexItemBuilder)).call(this));
  }

  createClass(FlexItemBuilder, [{
    key: 'createElement',
    value: function createElement(params) {
      return new FlexItem_1(params.parent);
    }
  }, {
    key: 'applyMetadata',
    value: function applyMetadata(params) {
      get(FlexItemBuilder.prototype.__proto__ || Object.getPrototypeOf(FlexItemBuilder.prototype), 'applyMetadata', this).call(this, params);

      this.applyProperties(params).applyContent(params);
    }

    /**
     * @protected
     * @param params
     * @return {FlexItemBuilder}
     */

  }, {
    key: 'applyProperties',
    value: function applyProperties(params) {
      var _this2 = this;

      var metadata = params.metadata;
      var element = params.element;

      var attributes = 'Align,Order,Grow,Shrink,Basis'.split(',');

      Object.keys(metadata).filter(function (name) {
        return attributes.some(function (prop) {
          return name.indexOf(prop) === 0;
        });
      }).forEach(function (name) {
        return element.setItemProperty(_this2.lowerFirstSymbol(name), metadata[name]);
      });

      return this;
    }

    /**
     * @protected
     * @param params
     * @return {FlexItemBuilder}
     */

  }, {
    key: 'applyContent',
    value: function applyContent(params) {
      var metadata = params.metadata;
      var element = params.element;
      var contentMetadata = metadata['Content'];

      element.setContent(this.getContentFactory(contentMetadata, params));

      return this;
    }

    /**
     * @protected
     * @param metadata
     * @param params
     * @return {function()}
     */

  }, {
    key: 'getContentFactory',
    value: function getContentFactory(metadata, params) {
      var element = params.element;
      var parentView = params.parentView;
      var builder = params.builder;

      if (!metadata) {
        return function () {};
      }

      return function () {
        var argumentForBuilder = {
          parent: element,
          parentView: parentView
        };

        return builder.build(metadata, argumentForBuilder);
      };
    }
  }]);
  return FlexItemBuilder;
}(ElementBuilder$1);

var FlexItemBuilder_1 = FlexItemBuilder;

var ApplicationBuilder = require$$0.ApplicationBuilder;

ApplicationBuilder.addToRegisterQueue('FlexContainer', new FlexContainerBuilder_1());
ApplicationBuilder.addToRegisterQueue('FlexItem', new FlexItemBuilder_1());

var main = { FlexContainer: FlexContainer_1, FlexItem: FlexItem_1, FlexContainerOptions: FlexContainerOptions, FlexItemOptions: FlexItemOptions, config: index };

return main;

}(InfinniUI));
