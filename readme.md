## Описание

Управление отображением элементов для [InfinniUI](https://github.com/InfinniPlatform/InfinniUI) на основе [FlexBox](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Flexible_Box_Layout).

Сборка: **"npm i && npm build"**

namespace: *InfinniFlex*

Доступные ресурсы: 
* InfinniFlex.FlexContainer
* InfinniFlex.FlexItem


При включении в проект *dist/infinni-flex.js* в InfinniUI.ApplicationBuilder регистрируются классы для построения элементов "FlexContainer" и "FlexItem".

## BreakPoints 

* xs *менее 600px*
* sm *от 600px*
* md *от 960px*
* lg *от 1280px* 
* xl *от 1920px и более* 

В метаданных *FlexContainer* и *FlexItem* параметры могут быть заданы с указанием *breakpoints* в виде:
 *"Параметр:BreakPoint"*: *"ЗначениеПараметра"*. 
 
Например в след. примере на размерах экрана менне чем *md* направление будет - "Колонка", а на размерах *md* и более - "Строка":
```json
{  
  "Direction": "Column",
  "Direction:md": "Row"    
}
``` 

## FlexContainer

Для построения регистрируется *InfinniFlex.FlexContainerBuilder*.

### Метаданные

#### Direction
см. [flex-direction](http://cssreference.io/property/flex-direction)
* Row
* RowReverse
* Column
* ColumnReverse

#### Wrap
см. [flex-wrap](http://cssreference.io/property/flex-wrap)
* NoWrap
* Wrap
* WrapReverse

#### Justify
см. [justify-content](http://cssreference.io/property/justify-content)
* Start
* End
* Center
* SpaceBetween
* SpaceAround
* Stretch

#### Align
см. [align-content](http://cssreference.io/property/align-content)
* Start
* End
* Center
* SpaceBetween
* SpaceAround
* Stretch

#### ChildrenAlign
см. [align-items](http://cssreference.io/property/align-items)
* Start
* End
* Center
* Baseline
* Stretch

#### Children

Массив метаданных элементов **FlexItem** 

### Пример метаданных контейнера

```json
{
  "FlexContainer": {
    "Direction": "Row",
    "Justify": "SpaceAround",
    "Align": "Stretch",
    "Wrap": "Wrap",
    "Wrap:md": "NoWrap",
    "ChildrenAlign": "Start",
    "Children": []
  }
}
```


## FlexItem

Для построения регистрируется *InfinniFlex.FlexItemBuilder*.

### Метаданные

#### Align
см. [align-self](http://cssreference.io/property/align-self)
* Auto
* Start
* End
* Center
* Baseline
* Stretch

#### Order
см. [order](http://cssreference.io/property/order)

#### Grow
см. [flex-grow](http://cssreference.io/property/flex-grow)

#### Shrink
см. [flex-shrink](http://cssreference.io/property/flex-shrink)

#### Basis
см. [flex-basis](http://cssreference.io/property/flex-basis)

#### Content

Метаданные визуального элемента платформы

### Пример метаданных контейнера

```json
{
  "FlexItem": {
    "Basis": "100%",
    "Shrink:lg": 2,
    "Content": {
      "Panel": {
        "Items": [
          {
            "TextBox": {
              "LabelText": "Text Box1"
            }
          }
        ]
      }
    }
  }
}
```

## Demo

see [live example](https://output.jsbin.com/wepegud) 